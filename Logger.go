package logger

type LoggerInterface interface {
	Info(msg string)
	Error(msg string)
}

var loggers []LoggerInterface

func Add(newLogger LoggerInterface) {
	loggers = append(loggers, newLogger)
}

func Info(msg string) {
	for _, logger := range loggers {
		logger.Info(msg)
	}
}

func Error(msg string) {
	for _, logger := range loggers {
		logger.Error(msg)
	}
}

