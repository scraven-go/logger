package logger

import "fmt"

type StdOutLogger struct {}

func (StdOutLogger) Info(msg string) {
	fmt.Printf("\n %s - %s", getDate(), msg)
}

func (StdOutLogger) Error(msg string) {
	fmt.Printf("\n %s - %s", getDate(), msg)
}